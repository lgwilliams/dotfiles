set encoding=utf-8

"Skip the shift key
no ; :

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'scrooloose/nerdtree'                         "File tree to switch and view files
Plugin 'vim-airline/vim-airline'                     "Fancy bar at the bottom of vim
Plugin 'vim-airline/vim-airline-themes'              "Fancy themes for the fancy bar
Plugin 'tpope/vim-fugitive'                          "Git wrapper for vim
Plugin 'yuttie/comfortable-motion.vim'               "Smooth scrolling
Plugin 'mhinz/vim-startify'                          "Nicer start screen for vim
Plugin 'ashisha/image.vim'                           "View image files in vim
Plugin 'suan/vim-instant-markdown', {'rtp': 'after'} "Preview markdown from vim
Plugin 'vimwiki/vimwiki'                             "For help with making docs
"Plugin 'ananagame/vimsence'                          "For discord rich presence
call vundle#end()            

" Allow markdown support
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}

" Bind a key to preview markdown
map <Leader>md :InstantMarkdownPreview<CR>

filetype plugin on

source ~/.vim/comments.vim

"Set the color theme to be used 
colorscheme sway

au BufEnter *dunstrc setf config

" Keybinding to show and hide the file tree
map <C-n> :NERDTreeToggle<CR>

" Inter config
" Split to the right to run the code
set splitright
" Set the browser to use for html files
let g:inter_browser = "qutebrowser"
" Load the plugin
source /home/luke/dev/vim/inter/inter.vim
" Save and run the plugin when the <F5> key is pressed
map <F5> :w<CR>:call Inter()<CR>

"Move lines up and down
nnoremap <C-k> :m .+1<CR>==
nnoremap <C-j> :m .-2<CR>==
inoremap <C-k> <Esc>:m .+1<CR>==gi
inoremap <C-j> <Esc>:m .-2<CR>==gi
vnoremap <C-k> :m '>+1<CR>gv=gv
vnoremap <C-j> :m '<-2<CR>gv=gv

"Resize splits
nnoremap <Leader><left> :vertical resize -5<cr>
nnoremap <Leader><down> :resize +5<cr>
nnoremap <Leader><up> :resize -5<cr>
nnoremap <Leader><right> :vertical resize +5<cr>

map <Leader><q> :quit<CR>

"Clear file
map <Leader>d :%d<CR> 

"Clear file
map <Leader>g :%y<CR> 

"Behave like vim instead of vi 
set nocompatible

"Automatically set the cwd
set autochdir
 
"Attempt to detect filetype/contents so that vim can autoindent etc 
filetype indent plugin on
 
"Enable syntax highlighting 
syntax on
 
"Enable switching from an  unsaved buffer without saving it first and keep an undo history for multiple files. Warn when quitting without saving, and keep swap files.
set hidden
 
"Better command-line completion 
set wildmenu
 
"Show partial commands in the last line of the screen
set showcmd
 
"Use case insensitive search, except when using capital letters
set ignorecase 
set smartcase
 
" Allow backspacing over autoindent, line breaks and start of insert action
set backspace=indent,eol,start
 
"When opening a new line and no filetype-specific indenting is enabled, keep the same indent as the line you're currently on(Useful for READMEs, etc)
set autoindent
 
"Instead of failing a command because of unsaved changes, instead raise a  dialogue asking if you wish to save changed files 
set confirm
 
"Display line numbers on the left
set number
 
"Quickly time out on keycodes, but never time out on mappings 
set notimeout ttimeout ttimeoutlen=200
 
"Use F11 to toggle between paste and nopaste
set pastetoggle=<F11>
 
"Enable firefox/google chrome like tab navigation
nmap <C-S-tab> :tabprevious<CR> 
nmap <C-tab> :tabnext<CR> 
map <C-S-tab> :tabprevious<CR> 
map <C-tab> :tabnext<CR> 
imap <C-S-tab> <Esc>:tabprevious<CR>i 
imap <C-tab> <Esc>:tabnext<CR>i 
nmap <C-t> :tabnew<CR> 
imap <C-t> <Esc>:tabnew<CR>

"Reload vimrc
map <C-i> :source ~/.vimrc<CR> 

"Share windows clipboard
set clipboard+=unnamed
 
"Indentation hardtab express settings
set shiftwidth=4 
set softtabstop=4 
set expandtab

"Set up welcome screen
let g:startify_bookmarks = ['~/.vimrc', '~/dev/python', '~/dev', '~/doc']
let g:startify_files_number = 3
let g:startify_lists = [
    \ { 'header': ['   Bookmarks'],       'type': 'bookmarks' },
    \ { 'header': ['   Recently Used '], 'type': 'files' },
    \ { 'header': ['   Recently Used in '. getcwd()], 'type': 'dir' },
    \ { 'header': ['   Sessions'],       'type': 'sessions' },
    \ ]

"Custom git commands
:command Gaddall !git add .

"Auto brace-matching
let g:xptemplate_brace_complete = '([{'
let g:xptemplate_vars = "SParg="

"Set the airline theme
let g:airline_powerline_fonts = 1
"if !exists('g:airline_symbols')
"	let g:airline_symbols = {}
"endif
"let g:airline_symbols.space = "\ua0"
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0

"let g:airline_theme='kolor'
let g:airline_theme='behelit'

"Set beam cursor
let &t_SI = "\<esc>[5 q"
let &t_SR = "\<esc>[5 q"
let &t_EI = "\<esc>[2 q"

"Set line length marker
"let file_name = buffer_name("%")
"if file_name =~ '\.py$'
    "set colorcolumn=80
    "highlight ColorColumn ctermbg=8
"endif

"Tabline configuration
let g:airline#extensions#tabline#fnamemod = ':t'

"Quickly go to the top and bottom of the file
map <C-Up> :goto 1 <CR>
map <C-Down> :$ <CR>

"Open up a command line from vim
map <F3> :!zsh<CR>

map <F7> :w<CR>:!ruby %<CR>
map <C-T> :tabnew<CR>:Startify<CR>

map <C-P> :vert term<CR>
