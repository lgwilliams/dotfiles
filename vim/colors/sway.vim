" Vim colorscheme template file
" Author: Luke
" Maintainer: Luke

set background=dark

highlight clear
if exists("syntax_on")
    syntax reset
endif
let g:colors_name="sway"

"Lightblue              45
"Darkblue               27
"DarkPurple             55
"Light purple           63
"Green                  48
"Orange                 166


"----------------------------------------------------------------
" General settings                                              |
"----------------------------------------------------------------
"----------------------------------------------------------------
" Syntax group   | Foreground    | Background    | Style        |
"----------------------------------------------------------------

" --------------------------------
" Editor settings
" --------------------------------
hi Normal          ctermfg=252     ctermbg=none     cterm=none
hi Cursor          ctermfg=none    ctermbg=none    cterm=none
hi CursorLine      ctermfg=none    ctermbg=none    cterm=none
hi LineNr          ctermfg=245     ctermbg=none    cterm=none
hi CursorLineNR    ctermfg=none    ctermbg=none    cterm=none

" -----------------
" - Number column -
" -----------------
hi CursorColumn    ctermfg=none    ctermbg=none    cterm=none
hi FoldColumn      ctermfg=none    ctermbg=none    cterm=none
hi SignColumn      ctermfg=none    ctermbg=none    cterm=none
hi Folded          ctermfg=none    ctermbg=none    cterm=none

" -------------------------
" - Window/Tab delimiters - 
" -------------------------
hi VertSplit       ctermfg=27      ctermbg=none    cterm=none
hi ColorColumn     ctermfg=none    ctermbg=none    cterm=none
hi TabLine         ctermfg=none    ctermbg=none    cterm=none
hi TabLineFill     ctermfg=none    ctermbg=none    cterm=none
hi TabLineSel      ctermfg=none    ctermbg=none    cterm=none

" -------------------------------
" - File Navigation / Searching -
" -------------------------------
hi Directory       ctermfg=27      ctermbg=none    cterm=none
hi Search          ctermfg=63      ctermbg=232     cterm=none
hi IncSearch       ctermfg=63    ctermbg=232    cterm=none

" -----------------
" - Prompt/Status -
" -----------------
hi WildMenu        ctermfg=none    ctermbg=none    cterm=none
hi Question        ctermfg=none    ctermbg=none    cterm=none
hi Title           ctermfg=none    ctermbg=none    cterm=none
hi ModeMsg         ctermfg=none    ctermbg=none    cterm=none
hi MoreMsg         ctermfg=none    ctermbg=none    cterm=none

" --------------
" - Visual aid -
" --------------
hi MatchParen      ctermfg=63    ctermbg=232    cterm=bold
hi Visual          ctermfg=63    ctermbg=232    cterm=none
hi VisualNOS       ctermfg=none    ctermbg=none    cterm=none
hi NonText         ctermfg=none    ctermbg=none    cterm=none

hi Todo            ctermfg=none    ctermbg=none    cterm=none
hi Underlined      ctermfg=none    ctermbg=none    cterm=none
hi Error           ctermfg=none    ctermbg=none    cterm=none
hi ErrorMsg        ctermfg=none    ctermbg=none    cterm=none
hi WarningMsg      ctermfg=none    ctermbg=none    cterm=none
hi Ignore          ctermfg=none    ctermbg=none    cterm=none
hi SpecialKey      ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Variable types
" --------------------------------
hi Constant        ctermfg=118     ctermbg=none    cterm=none
hi String          ctermfg=42      ctermbg=none    cterm=none
hi StringDelimiter ctermfg=none    ctermbg=none    cterm=none
hi Character       ctermfg=none    ctermbg=none    cterm=none
hi Number          ctermfg=92      ctermbg=none    cterm=none
hi Boolean         ctermfg=92      ctermbg=none    cterm=none
hi Float           ctermfg=92      ctermbg=none    cterm=none

hi Identifier      ctermfg=71      ctermbg=none    cterm=none
hi Function        ctermfg=27    ctermbg=none    cterm=bold

" --------------------------------
" Language constructs
" --------------------------------
hi Statement       ctermfg=63      ctermbg=none    cterm=none
hi Conditional     ctermfg=27      ctermbg=none    cterm=none
hi Repeat          ctermfg=27    ctermbg=none    cterm=none
hi Label           ctermfg=27    ctermbg=none    cterm=none
hi Operator        ctermfg=27    ctermbg=none    cterm=none
hi Keyword         ctermfg=63      ctermbg=none    cterm=none
hi Exception       ctermfg=63      ctermbg=none    cterm=none
hi Comment         ctermfg=243     ctermbg=none    cterm=none

hi Special         ctermfg=none    ctermbg=none    cterm=none
hi SpecialChar     ctermfg=none    ctermbg=none    cterm=none
hi Tag             ctermfg=none    ctermbg=none    cterm=none
hi Delimiter       ctermfg=none    ctermbg=none    cterm=none
hi SpecialComment  ctermfg=none    ctermbg=none    cterm=none
hi Debug           ctermfg=none    ctermbg=none    cterm=none

" ----------
" - C like -
" ----------
hi PreProc         ctermfg=92      ctermbg=none    cterm=none
hi Include         ctermfg=92      ctermbg=none    cterm=none
hi Define          ctermfg=92      ctermbg=none    cterm=none
hi Macro           ctermfg=none    ctermbg=none    cterm=none
hi PreCondit       ctermfg=none    ctermbg=none    cterm=none

hi Type            ctermfg=63      ctermbg=none    cterm=none
hi StorageClass    ctermfg=63    ctermbg=none    cterm=none
hi Structure       ctermfg=92    ctermbg=none    cterm=none
hi Typedef         ctermfg=63    ctermbg=none    cterm=none

" --------------------------------
" Diff
" --------------------------------
hi DiffAdd         ctermfg=28    ctermbg=none      cterm=none
hi DiffChange      ctermfg=63    ctermbg=none    cterm=none
hi DiffDelete      ctermfg=124    ctermbg=none    cterm=none
hi DiffText        ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Completion menu
" --------------------------------
hi Pmenu           ctermfg=249     ctermbg=235     cterm=none
hi PmenuSel        ctermfg=235     ctermbg=249     cterm=none
hi PmenuSbar       ctermfg=none    ctermbg=none    cterm=none
hi PmenuThumb      ctermfg=none    ctermbg=none    cterm=none

" --------------------------------
" Spelling
" --------------------------------
hi SpellBad        ctermfg=none    ctermbg=none    cterm=none
hi SpellCap        ctermfg=none    ctermbg=none    cterm=none
hi SpellLocal      ctermfg=none    ctermbg=none    cterm=none
hi SpellRare       ctermfg=none    ctermbg=none    cterm=none

"--------------------------------------------------------------------
" Specific settings                                                 |
"--------------------------------------------------------------------
