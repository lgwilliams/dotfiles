# Suspend dunst and lock, then resume dunst when unlocked.
pkill -u $USER -USR1 dunst
i3lock -n -i /home/luke/.config/bspwm/lock.jpg \
    --insidecolor=353e4cff --ringcolor=202129ff --line-uses-inside \
    --keyhlcolor=04bd67ff --bshlcolor=4b5769ff --separatorcolor=353e4cff \
    --insidevercolor=4b5769ff --insidewrongcolor=202129ff \
    --ringvercolor=04bd67ff --ringwrongcolor=353e4cff --indpos="x+240:y+961" \
    --radius=15 --veriftext="" --wrongtext="" --noinputtext=""
pkill -u $USER -USR2 dunst
